# Airspace Frenzy

[![Build Status](https://joshuagoodw.in/jenkins/buildStatus/icon?job=airspacefrenzy6)](https://joshuagoodw.in/jenkins/job/airspacefrenzy6/)

## Setup

### Git

If you want to commit anything to this repository, you need to use Git. [SourceTree](http://www.sourcetreeapp.com/) is thought to work particularly with Bitbucket; [GitHub for Windows](http://windows.github.com/) and [for Mac](http://mac.github.com/) are nice if you don't mind sometimes being nagged for your GitHub login details (which are not required). There are different things installed in the CS labs.

If you're using Linux or you want to use the terminal, there's lots of documentation about Git on the [Git website](http://git-scm.com/documentation).

### Maven

This project uses Maven as its build system. If you have a copy installed, you can just run

    mvn package

and Maven will download and build everything for you - producing a nice JAR at the end. 

For most IDEs, you should be able to import this project as an existing Maven project.

### Eclipse on CS Machines

The Maven "m2e" plugin for Eclipse can be installed by going to Help -> Install New Software... In the "Work with:" box type `http://download.eclipse.org/releases/juno/` and press enter (or click ▼ and select it from the menu). The m2e plugin can be found under "General Purpose Tools" -> "m2e - Maven Integration for Eclipse".

Once the plugin is installed, simply import the project into Eclipse and it should build. The first time you do this, it may take some time since all the dependencies must be downloaded from the internet. You may need to right-click on the project in the Package Explore pane, and click "Maven" -> "Update Project...".
