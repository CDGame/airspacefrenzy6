package seprhou.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Implementation of the Screen interface which sets up classes common to all screens
 */
public abstract class AbstractScreen implements Screen
{
	public static final int SCREEN_WIDTH = 1680;
	public static final int SCREEN_HEIGHT = 1050;

	private final AtcGame game;
	private final Stage stage;
	private final boolean enableEscape;
	private final boolean enableSpace;
	private final boolean enableAnyKey;

	private final FPSLogger logger = new FPSLogger();
	
	//escPressed and keyPressed are flags that determine whether Escape or any other key respectively have been pressed yet IN THIS SCREEN
	private boolean escPressed, spacePressed, keyPressed = false;
	
	//currentlyPaused is used as a flag to determine whether to regenerate the game or keep the existing instance (in GameScreen.java)
	private static boolean currentlyPaused = false;
	
	/**
	 * Creates a new AbstractScreen owned by a game
	 *
	 * @param game game which owns this screen
	 */
	public AbstractScreen(AtcGame game)
	{
		this(game, true, true, true);
	}

	/**
	 * Creates a new screen which is owned by the game
	 *
	 * @param game game which owns this screen
	 * @param enableEscape true to enable the escape key (go back to main menu)
	 */
	public AbstractScreen(AtcGame game, boolean enableEscape, boolean enableSpace, boolean anyKeyToMenu)
	{
		this.game = game;
		this.enableEscape = enableEscape;
		this.enableSpace = enableSpace;
		this.enableAnyKey = anyKeyToMenu;
		// Create stage to draw everything with
		this.stage = new Stage(SCREEN_WIDTH, SCREEN_HEIGHT);
		this.stage.addListener(new InputListener()
		{
			//Listens for when the keys are pressed
			@Override
            public boolean keyDown(InputEvent event, int keycode)
			{
				if (keycode == Input.Keys.ESCAPE)
				{
					System.out.println("Esc Pressed");
					escPressed = true;
					return true;
				}
				else if (keycode == Input.Keys.SPACE)
				{
					System.out.println("Space Pressed");
					spacePressed = true;
					return true;
				}
				else
				{
					System.out.println("Key Pressed");
					keyPressed = true;
					return true;
				}

			}
		});

	}
	/** Returns the game which owns this screen */
	public AtcGame getGame()
	{
		return game;
	}

	/** Returns the stage where everything is drawn from */
	public Stage getStage()
	{
		return stage;
	}
	
	/*
	public void setAnyKey(boolean anyKey)
	{
		this.enableAnyKey = anyKey;
		return;
	}
	*/

	@Override
	public void show()
	{
		// Called when the screen is re-shown after being hidden

		// Setup cursor
		int xOffset = Assets.CURSOR_IMAGE.getWidth() / 2;
		int yOffset = Assets.CURSOR_IMAGE.getHeight() / 2;
		Gdx.input.setCursorImage(Assets.CURSOR_IMAGE, xOffset, yOffset);

		// Setup input processor (which is probably owned by another screen currently)
		Gdx.input.setInputProcessor(this.stage);
		
		//reset the flags for escape and key pressed
		escPressed = false;
		spacePressed = false;
		keyPressed = false;
	}

	@Override
	public void render(float delta)
	{
		// Clears the screen with the given RGB colour (black)
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
		
		// If escape has just been released...
		if (enableEscape && escPressed)
		{
			// If the current screen is the game screen, pause
			if (this.getClass().getSimpleName().contains("GameScreen"))
			{
				System.out.println("pause");
				setCurrentlyPaused(true);
				game.showPause();
			}
			// Otherwise, go back to the menu
			else
			{
				System.out.println("quit");
				setCurrentlyPaused(false);
				game.showMenu();
			}
			return;
		}

		// If space has just been released...
		if (enableSpace && spacePressed)
		{
			//If the game screen is the pause screen, return to the game
			if (this.getClass().getSimpleName().contains("PauseScreen"))
			{
				game.showGame();
			}
			// Otherwise, go back to the menu
			else
			{
				game.showMenu();
			}
			return;

		}
		
		// If any key is pressed...
		else if (enableAnyKey && keyPressed)
		{
			//If the game screen is the pause screen, return to the game
			if (this.getClass().getSimpleName().contains("PauseScreen"))
			{
				game.showGame();
			}
			/*
			// Otherwise, go back to the menu
			else
			{
				game.showMenu();
			}
			*/
			return;
		}
		

		// Update game state
		stage.act(delta);

		// Render game
		stage.draw();
		
		// Record frames per second and output to console
		//logger.log();
	}
	
	@Override
	public void resize(int arg0, int arg1)
	{
		// Makes the stage the same size as the camera and changes the actors' sizes in proportion
		stage.setViewport(SCREEN_WIDTH, SCREEN_HEIGHT);
	}

	// Empty methods
	@Override public void dispose() { }
	@Override public void hide() { }
	@Override public void pause() { }
	@Override public void resume() { }

	public boolean isCurrentlyPaused() {
		return currentlyPaused;
	}

	public void setCurrentlyPaused(boolean currentlyPaused) {
		AbstractScreen.currentlyPaused = currentlyPaused;
	}
}
