package seprhou.gui;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

/**
 * The main class of the game
 *
 * <p>
 * This class is used for all incoming events (like {@link com.badlogic.gdx.Game#render()}
 * from libGDX. After the game is setup, all the events received by this class
 * are simply passed to the current screen.
 */
public class AtcGame extends Game
{
	private Screen menuScreen, gameScreen, scoresScreen;
	private GameOverScreen gameOverScreen;
	private PauseScreen pauseScreen;

	@Override
	public void create()
	{
		// Set initial screen
		showMenu();
	}

	/** Show the menu screen */
	public void showMenu()
	{
		if (menuScreen == null)
			menuScreen = new MenuScreen(this);
		System.out.println("Showing menu");
		setScreen(menuScreen);
	}

	/** Show the game screen */
	public void showGame()
	{
		if (gameScreen == null)
			gameScreen = new GameScreen(this);
		setScreen(gameScreen);
	}

	/** Show the high scores screen */
	public void showHighScores()
	{
		if (scoresScreen == null)
			scoresScreen = new HighScoresScreen(this);
		setScreen(scoresScreen);
	}
	
	/**
	 * Show the game over screen
	 *
	 * @param time the number of seconds elapsed when the game finished
	 */
	public void showGameOver(float time, int score)
	{
		if (gameOverScreen == null)
			gameOverScreen = new GameOverScreen(this);

		gameOverScreen.setLabelValue(time, score);
		System.out.println("Showing game over");
		setScreen(gameOverScreen);
	}
	
	/** Show the pause screen */
	public void showPause()
	{
		if (pauseScreen == null)
			pauseScreen = new PauseScreen(this);
		setScreen(pauseScreen);
	}
}
