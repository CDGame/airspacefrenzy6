package seprhou.gui;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import seprhou.logic.Aircraft;
import seprhou.logic.Utils;

/**
 * The actor which displays the control panel on the right of the game screen
 */
public class ControlPanel extends Group
{
	private final GameScreen parent;

	// data to display
	private final Label valueFlightNo, valueAltitude, valueBearing, valueAirspeed, valueXPosition, valueYPosition, valueCrew, valueWeight;

	private Label timerLabel;
	private final Label scoreLabel;

	// positions for drawing items
	private final float nameXAlign = 40.0f, valueXAlign = 60.0f, titleScale = 0.6f;
	private float yBottom = 850.0f; // decreases as each item is drawn going down the screen


	/**
	 * Creates a new ControlPanel
	 *
	 * @param parent parent GameScreen
	 */
	public ControlPanel(GameScreen parent)
	{
		this.parent = parent;
				
		Image gamelogo = new Image(Assets.GAME_TITLE);
		gamelogo.setX(this.nameXAlign);
		gamelogo.setY(900.0f);
		this.addActor(gamelogo);

		valueFlightNo = new Label("", Assets.SKIN);
		this.addItem("Flight Number:", valueFlightNo);
		
		valueAltitude = new Label("", Assets.SKIN);
		this.addItem("Altitude:", valueAltitude);
		
		valueBearing = new Label("", Assets.SKIN);
		this.addItem("Bearing:", valueBearing);

		valueAirspeed = new Label("", Assets.SKIN);
		this.addItem("Speed:", valueAirspeed);
		
		valueXPosition = new Label("", Assets.SKIN);
		valueYPosition = new Label("", Assets.SKIN);
		this.addItem("Position:", valueXPosition, valueYPosition);
		
		valueCrew = new Label("", Assets.SKIN);
		this.addItem("Crew:", valueCrew);
		
		valueWeight = new Label("", Assets.SKIN);
		addItem("Weight:", valueWeight);

		Label nameTimer = new Label("Time:", Assets.SKIN);
		nameTimer.setX(nameXAlign);
		nameTimer.setY(220.0f);
		nameTimer.setFontScale(this.titleScale);
		this.addActor(nameTimer);

		timerLabel = new Label("", Assets.SKIN);
		timerLabel.setX(valueXAlign);
		timerLabel.setY(200.0f);
		timerLabel.setFontScale(1.6f);
		this.addActor(timerLabel);
		
		Label nameScore = new Label("Score:", Assets.SKIN);
		nameScore.setX(nameXAlign);
		nameScore.setY(120.0f);
		nameScore.setFontScale(this.titleScale);
		this.addActor(nameScore);
		
		scoreLabel = new Label("", Assets.SKIN);
		scoreLabel.setX(valueXAlign);
		scoreLabel.setY(100.0f);
		scoreLabel.setFontScale(1.6f);
		this.addActor(scoreLabel);
	
	}

	/**
	 * Returns the game screen used by the panel
	 */
	public GameScreen getGameScreen()
	{
		return parent;
	}

	@Override
	public void act(float delta)
	{
		Aircraft selected = this.parent.getSelectedAircraft();
		if (selected != null)
		{
			valueFlightNo.setText(selected.getName());
			valueAltitude.setText(Integer.toString(Math.round(selected.getAltitude())) + " ft");
			valueBearing.setText(Integer.toString(Math.round(selected.getBearing())) + " degrees");
			valueAirspeed.setText(Integer.toString(Math.round(selected.getVelocity().getLength() * 10))+ " mph");
			valueXPosition.setText("x = "+ Math.round(selected.getPosition().getX()));
			valueYPosition.setText("y = "+ Math.round(selected.getPosition().getY()));
			valueCrew.setText(Integer.toString(selected.getCrew()));
			valueWeight.setText(Integer.toString(Math.round(selected.getWeight())) + " tonnes");
		}
		else
		{
			valueFlightNo.setText("");
			valueAltitude.setText("");
			valueBearing.setText("");
			valueAirspeed.setText("");
			valueXPosition.setText("");
			valueYPosition.setText("");
			valueCrew.setText("");
			valueWeight.setText("");
		}

		// Update timer
		timerLabel.setText(Utils.formatTime(parent.getSecondsSinceStart()));
		
		scoreLabel.setText(Integer.toString(parent.getScore()));
	}
	
	/*
	 * Add an item with a single value
	 * 
	 * @param name       The name of the item, drawn on the screen
	 * @param valueLabel An existing label, where the number is written
	 */
	private void addItem(String name, Label valueLabel) {
		Label nameLabel = new Label(name, Assets.SKIN);
		nameLabel.setX(this.nameXAlign);
		nameLabel.setY(this.yBottom);
		nameLabel.setFontScale(this.titleScale);
		this.addActor(nameLabel);
	
		this.yBottom -= 20.0f;
	
		valueLabel.setX(this.valueXAlign);
		valueLabel.setY(this.yBottom);
		this.addActor(valueLabel);
		
		this.yBottom -= 60.0f;
	}
	
	/*
	 * Add an item with two parts to the value (for example, x position and y position)
	 * 
	 * @param name             The name of the item, drawn on the screen
	 * @param firstValueLabel  The first value drawn
	 * @param secondValueLabel The second value drawn underneath

	 */
	private void addItem(String name, Label firstValueLabel, Label secondValueLabel) {
		// the item name, and the first value
		addItem(name, firstValueLabel);
		
		// move cursor back up to draw the second label
		this.yBottom += 30.0f;
		
		// the second value
		secondValueLabel.setX(this.valueXAlign);
		secondValueLabel.setY(this.yBottom);
		this.addActor(secondValueLabel);

		// reinstate the bottom margin
		this.yBottom -= 60.0f;
	}
}
