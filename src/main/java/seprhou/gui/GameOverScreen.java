package seprhou.gui;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import seprhou.logic.HighScores;
import seprhou.logic.Utils;

/**
 * The screen displayed after the game is over
 */
public class GameOverScreen extends AbstractScreen
{
	private final Label screenLabel, nameLabel;
	private final TextField nameField;
	private final TextButton submitButton;
	private int score;
	
	
	public GameOverScreen(final AtcGame game)
	{
		super(game, true, true, false);
		Stage stage = getStage();

		// Background image
		Image gameOverImage = new Image(Assets.GAMEOVER_TEXTURE);
		stage.addActor(gameOverImage);

		// Final timer, score value shown on the screen
		screenLabel = new Label("", Assets.SKIN);
		screenLabel.setPosition(500, 300);
		stage.addActor(screenLabel);

		// Label for the name
		nameLabel = new Label("Enter your name:", Assets.SKIN);
		nameLabel.setPosition(810, 200);
		stage.addActor(nameLabel);

		// Field for the name
		nameField = new TextField("", Assets.SKIN);
		nameField.setPosition(855, 140);
		stage.addActor(nameField);		

		// Button to submit high score
		submitButton = new TextButton("Submit Score", Assets.SKIN);
		submitButton.setPosition(835, 80);
		submitButton.addListener(new ClickListener() {
            @Override public void clicked(InputEvent event, float x, float y) {
            	HighScores highScores = new HighScores();
            	highScores.addScore(nameField.getText(), score);
            	getGame().showMenu();
            };
        });
		stage.addActor(submitButton);	
		
	}

	/**
	 * Sets the number of seconds to show on the game screen as the final time
	 *
	 * @param value time to show in seconds
	 */
	public void setLabelValue(float value, int score)
	{
		this.score = score;
		
		// Update screen label
		screenLabel.setText("Your final time was: " + Utils.formatTime(value) + "\n" +
				"Your score: " + score + "\n" +
				"Press SPACE to return to the main menu");
	}

	
}
