package seprhou.gui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import seprhou.logic.*;

/**
 * The main game screen
 *
 * <p>
 * The GameScreen consists of the GameArea (on the left) and the ControlPanel (on the right).
 * These classes control drawing of the game itself and the GameArea controls updates due to
 * interactions (clicking and key presses). The Airspace itself and the current game state is
 * controlled from this class however.
 */
public class GameScreen extends AbstractScreen
{
	private static final FlightPlanGenerator flightPathGenerator;

	private final GameArea gameArea;
	private final ControlPanel controlPanel;

	private Airspace airspace;
	private Aircraft selectedAircraft;
	private float secondsSinceStart;
	//private int score = 0;

	static
	{
		flightPathGenerator = new FlightPlanGenerator();
		flightPathGenerator.setWaypoints(Constants.WAYPOINTS);
		flightPathGenerator.setEntryExitPoints(Constants.ENTRY_EXIT_POINTS);
		flightPathGenerator.setInitialAltitudes(Constants.INITIAL_ALTITUDES);
		flightPathGenerator.setInitialSpeeds(Constants.INITIAL_SPEEDS);
		flightPathGenerator.setMinSafeEntryDistance(Constants.MIN_SAFE_ENTY_DISTANCE);
		flightPathGenerator.setMinTimeBetweenAircraft(Constants.MIN_TIME_BETWEEN_AIRCRAFT);
		flightPathGenerator.setAircraftPerSec(Constants.AIRCRAFT_PER_SEC);
		flightPathGenerator.setMaxAircraft(Constants.MAX_AIRCRAFT);
		flightPathGenerator.setMinWaypoints(Constants.MIN_WAYPOINTS);
		flightPathGenerator.setMaxWaypoints(Constants.MAX_WAYPOINTS);
	}

	public GameScreen(AtcGame game)
	{
		super(game, true, false, false);
		Stage stage = getStage();

		// Background image
		Image background = new Image(Assets.BACKGROUND_TEXTURE);
		stage.addActor(background);

		// Create the game area (where the action takes place)
		gameArea = new GameArea(this);
		gameArea.setBounds(0, 0, 1400, SCREEN_HEIGHT);
		stage.addActor(gameArea);

		// Create the control panel (where info is displayed)
		controlPanel = new ControlPanel(this);
		controlPanel.setBounds(1400, 0, 280, SCREEN_HEIGHT);
		stage.addActor(controlPanel);

		// Add timer update actor
		stage.addActor(new Actor()
		{
			@Override
			public void act(float delta)
			{
				secondsSinceStart += delta;
			}
		});
		
		/*
		stage.addActor(new Actor()
		{
			public void act(int points)
			{
				score += points; //until score function is added
			}
		});
		
		*/
		
		// Give the game area keyboard focus (this is where keyboard events are sent)
		stage.setKeyboardFocus(gameArea);
	}

	@Override
	public void show()
	{
		super.show();
		//If the game is paused, the game is not reset (only shown again).
		if (isCurrentlyPaused() == false)
		{
			// Create the airspace
			airspace = new Airspace();
			airspace.setDimensions(new Rectangle(gameArea.getWidth(), gameArea.getHeight()));
			airspace.setLateralSeparation(Constants.LATERAL_SEPARATION);
			airspace.setVerticalSeparation(Constants.VERTICAL_SEPARATION);
			airspace.setObjectFactory(new AirspaceObjectFactory()
			{
				@Override
				public AirspaceObject makeObject(Airspace airspace, float delta)
				{
					FlightPlan flightPlan = flightPathGenerator.makeFlightPlan(airspace, delta);
	
					if (flightPlan != null)
					{
						// Random flight number between YO000 and YO999
						String flightNumber = String.format("YO%03d", Utils.getRandom().nextInt(1000));
	
						return new ConcreteAircraft(flightNumber, 100, 5, flightPlan);
					}
	
					return null;
				}
			});
	
			// Reset information from past games
			selectedAircraft = null;
			//score = 0;
			secondsSinceStart = 0;
		}
	}
	
	@Override
	public void hide()
	{ 
		super.hide();
	}

	/** Returns the airspace object used by the game */
	public Airspace getAirspace()
	{
		return airspace;
	}

	/**
	 * Returns the currently selected aircraft or null if nothing is selected
	 */
	public Aircraft getSelectedAircraft()
	{
		return selectedAircraft;
	}

	/**
	 * Returns the number of seconds since the start of this game
	 */
	public float getSecondsSinceStart()
	{
		return secondsSinceStart;
	}
	
	
	public int getScore()
	{
		return getAirspace().getScore();
	}

	
	/**
	 * Sets the currently selected aircraft
	 *
	 * @param selectedAircraft new current aircraft or null for no selected aircraft
	 */
	public void setSelectedAircraft(Aircraft selectedAircraft)
	{
		this.selectedAircraft = selectedAircraft;
	}
}
