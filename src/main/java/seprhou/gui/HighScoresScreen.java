package seprhou.gui;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import seprhou.logic.*;

/**
 * Class which is used to show the high scores
 */
public class HighScoresScreen extends AbstractScreen
{
	private final Label title, scoresLabel, escapeLabel;
	
	public HighScoresScreen(AtcGame game)
	{
		super(game, true, false, false);
		Stage stage = getStage();

		// Set background image
		Image backgroundImage = new Image(Assets.MENU_BACKGROUND_TEXTURE);
		stage.addActor(backgroundImage);
		
		title = new Label("High scores:", Assets.SKIN);
		title.setX(200.0f);
		title.setY(640.0f);
		stage.addActor(title);
		
		HighScores highScores = new HighScores();
		
		scoresLabel = new Label(highScores.getHighscoreString(), Assets.SKIN);
		scoresLabel.setX(200.0f);
		scoresLabel.setY(250.0f);
		scoresLabel.setFontScale(1.0f);
		stage.addActor(scoresLabel);
		
		escapeLabel = new Label("Press ESC to return to the main menu", Assets.SKIN);
		escapeLabel.setX(300.0f);
		escapeLabel.setY(150.0f);
		stage.addActor(escapeLabel);


	}
}
