package seprhou.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * The screen displaying the main menu
 */
public class MenuScreen extends AbstractScreen
{
	private static final float BUTTON_WIDTH = 300f;
	private static final float BUTTON_HEIGHT = 60f;
	private static final float BUTTON_SPACING = 15f;
	
	// Put buttons in the middle of the screen in the x-axis
	private static final float BUTTON_X = (SCREEN_WIDTH - BUTTON_WIDTH) / 2;
	
	// Start placing the label and buttons 100px above the centre of the screen in the y-axis
	private static float currentY = ((SCREEN_HEIGHT - BUTTON_HEIGHT)/2);

	/**
	 * Initialises the menu screen
	 *
	 * @param game game which owns this screen
	 */
	public MenuScreen(AtcGame game)
	{
		super(game, false, false, false);
		Stage stage = getStage();

		// Clear the stage
		stage.clear();

		// Set background image
		Image backgroundImage = new Image(Assets.MENU_BACKGROUND_TEXTURE);
		stage.addActor(backgroundImage);

		// button "Start Game"
		addButton("Start Game", new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				getGame().showGame();
			}
		}, stage);

		// button "High Scores"
		addButton("High Scores", new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				getGame().showHighScores();
			}
		}, stage);
		
		// button "Exit"
		addButton("Exit", new ClickListener() {	
			@Override
			public void clicked(InputEvent event, float x, float y)
			{
				Gdx.app.exit();
			}
		}, stage);
	}
	
	/**
	 * Adds a button to the screen
	 * 
	 * @param text     text written on the button
	 * @param listener listener fired when the button is clicked
	 * @param stage    stage to add the button to
	 */
	private void addButton(String text, ClickListener listener, Stage stage)
	{
		TextButton newButton = new TextButton(text, Assets.SKIN);
		newButton.setX(BUTTON_X);
		newButton.setY((currentY -= BUTTON_HEIGHT + BUTTON_SPACING));
		newButton.setWidth(BUTTON_WIDTH);
		newButton.setHeight(BUTTON_HEIGHT);
		newButton.addListener(listener);
		stage.addActor(newButton);
	}
}