package seprhou.gui;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * The screen displayed after pausing the game
 */
public class PauseScreen extends AbstractScreen
{
	private final Label excapeLabel, resumeLabel;

	public PauseScreen(AtcGame game)
	{
		super(game, true, true, true);
		Stage stage = getStage();

		// Background image
		Image backgroundImage = new Image(Assets.MENU_BACKGROUND_TEXTURE);
		stage.addActor(backgroundImage);
		
		excapeLabel = new Label("Press ESC to return to the main menu", Assets.SKIN);
		excapeLabel.setPosition(600.0f, 400.0f);
		stage.addActor(excapeLabel);
		
		resumeLabel = new Label("Press any other key to resume play", Assets.SKIN);
		resumeLabel.setPosition(600.0f, 340.0f);
		stage.addActor(resumeLabel);

	}
}
