package seprhou.logic;

import java.util.*;

import seprhou.gui.Constants;

/**
 * Controls an entire air space and all the aircraft in it
 *
 * <p>
 * The game logic is designed to be polled each frame for events rather than
 * call callback methods. Each frame, you should call the {@link #refresh(float)}
 * method, and then use the other methods to observe the new state of the game.
 */
public class Airspace
{
	private AirspaceObjectFactory objectFactory;
	private Rectangle dimensions;
	private float lateralSeparation, verticalSeparation;
	private int score = 0;

	private ArrayList<AirspaceObject> culledObjects = new ArrayList<>();
	private ArrayList<CollisionWarning> collisionWarnings = new ArrayList<>();

	/** During the game refresh, this list is sorted so that LOWER planes (altitude) are put FIRST */
	private ArrayList<AirspaceObject> activeObjects = new ArrayList<>();

	private ArrayList<AirspaceObject> circlingObjects = new ArrayList<>();
	private ArrayList<AirspaceObject> landingObjects = new ArrayList<>();
	private ArrayList<AirspaceObject> takingOffObjects = new ArrayList<>();
	
	private boolean gameOver;

	/** Returns the factory responsible for constructing airspace objects */
	public AirspaceObjectFactory getObjectFactory()
	{
		return objectFactory;
	}

	/**
	 * Sets the class used by this airspace for creating aircraft
	 *
	 * @param factory new object factory (not null)
	 */
	public void setObjectFactory(AirspaceObjectFactory factory)
	{
		if (factory == null)
			throw new IllegalArgumentException("factory cannot be null");

		this.objectFactory = factory;
	}

	/** Returns the dimensions of the airspace */
	public Rectangle getDimensions()
	{
		return dimensions;
	}

	/**
	 * Sets the dimensions of the game area to the given rectangle
	 *
	 * @param dimensions new airspace dimensions
	 */
	public void setDimensions(Rectangle dimensions)
	{
		if (dimensions == null)
			throw new IllegalArgumentException("dimensions cannot be null");

		this.dimensions = dimensions;
	}

	/**
	 * Returns the lateral separation distance to generate warnings at
	 *
	 * <p>This only affects the collision warnings. Actual crashes are determined from the aircraft sizes
	 *
	 * @see AirspaceObject#getSize()
	 */
	public float getLateralSeparation()
	{
		return lateralSeparation;
	}

	/**
	 * Sets the new lateral separation distance
	 *
	 * @param separation lateral separation distance to generate warnings at
	 * @see #getLateralSeparation()
	 */
	public void setLateralSeparation(float separation)
	{
		if (separation <= 0)
			throw new IllegalArgumentException("separation must be greater than 0");

		this.lateralSeparation = separation;
	}

	/**
	 * Returns the vertical separation distance to generate warnings at
	 *
	 * <p>This only affects the collision warnings. Actual crashes are determined from the aircraft sizes
	 *
	 * @see AirspaceObject#getSize()
	 */
	public float getVerticalSeparation()
	{
		return verticalSeparation;
	}

	/**
	 * Sets the new vertical separation distance
	 *
	 * @param separation vertical separation distance to generate warnings at
	 * @see #getVerticalSeparation()
	 */
	public void setVerticalSeparation(float separation)
	{
		if (separation <= 0)
			throw new IllegalArgumentException("separation must be greater than 0");

		this.verticalSeparation = separation;
	}

	/**
	 * Throws an exception if the class has not been initialized properly
	 */
	private void ensureInitialized()
	{
		if (objectFactory == null || dimensions == null ||
				lateralSeparation == 0 || verticalSeparation == 0)
		{
			throw new IllegalStateException("The Airspace class must be initialized before use");
		}
	}

	/**
	 * Returns the aircraft which were culled during the last refresh
	 *
	 * <p>
	 * This is the list of aircraft which flew off the screen and
	 * were therefore culled / destroyed.
	 *
	 * <p>
	 * The returned list is unmodifiable and is reused each refresh (so you must
	 * copy it if you want to keep it between refreshes)
	 */
	public Collection<AirspaceObject> getCulledObjects()
	{
		return Collections.unmodifiableCollection(culledObjects);
	}

	/** Returns the list of objects circling the airport */
	public ArrayList<AirspaceObject> getCirclingObjects() {
		return circlingObjects;
	}
	
	/**
	 * Returns the list of active aircraft
	 *
	 * <p>These objects are sorted by altitude (small altitudes first)
	 */
	public Collection<AirspaceObject> getActiveObjects()
	{
		return activeObjects;
	}

	/**
	 * Returns the list of collision warnings generated during the last refresh
	 *
	 * <p>
	 * The returned list is unmodifiable and is reused each refresh (so you must
	 * copy it if you want to keep it between refreshes)
	 */
	public Collection<CollisionWarning> getCollisionWarnings()
	{
		return Collections.unmodifiableCollection(collisionWarnings);
	}

	/**
	 * Finds the aircraft which is occupying the given point
	 *
	 * <p>
	 * This method takes into account the size of aircraft to determine where they are.
	 * If multiple aircraft occupy a point, the one with the HIGHEST altitude will be chosen
	 *
	 * @param centre point to start search at
	 * @return the aircraft found occupying the given point
	 */
	public AirspaceObject findAircraft(Vector2D centre)
	{
		// Iterate through all objects IN REVERSE and return the first found
		for (int i = activeObjects.size() - 1; i >= 0; i--)
		{
			AirspaceObject current = activeObjects.get(i);
			float distance = current.getPosition().distanceTo(centre);

			if (distance < current.getSize())
				return current;
		}

		return null;
	}

	/**
	 * Finds the aircrafts whose centres are within the given circle
	 *
	 * <p>
	 * Objects are ordered with highest altitude first
	 *
	 * @param centre the centre of the circle to search
	 * @param radius the radius of the circle to search
	 * @return the list of aircraft found - may be empty of no aircraft were found
	 */
	public List<AirspaceObject> findAircraft(Vector2D centre, float radius)
	{
		ArrayList<AirspaceObject> results = new ArrayList<>();

		// Iterate through all objects IN REVERSE and return the accepted objects
		for (int i = activeObjects.size() - 1; i >= 0; i--)
		{
			AirspaceObject current = activeObjects.get(i);
			float distance = current.getPosition().distanceTo(centre);

			if (distance < radius)
				results.add(current);
		}

		return results;
	}

	/** Returns true if the game is over */
	public boolean isGameOver()
	{
		return gameOver;
	}

	/** Culls objects outside the game area */
	private void cullObjects()
	{
		Rectangle gameArea = getDimensions();

		culledObjects.clear();

		// Test if every object is within the area and cull if not
		// Test if every landing object has landed and cull if it has
		//  Do in reverse in case multiple objects are culled
		for (int i = activeObjects.size() - 1; i >= 0; i--)
		{
			AirspaceObject object = activeObjects.get(i);

			if (!gameArea.intersects(object.getPosition(), object.getSize()))
			{
				activeObjects.remove(i);
				culledObjects.add(object);
			}
			
			if (landingObjects.contains(object) && object.getAltitude() == 0)
			{
				activeObjects.remove(i);
				circlingObjects.remove(object);
				landingObjects.remove(object);
				culledObjects.add(object);
				score += Constants.POINTS_FOR_LANDING;
			}
		}
	}

	/** Generates the list of collision warnings */
	private void calculateCollisions()
	{
		// This is a simple collision algorithm, going through all objects
		// and checking them with all other objects.
		// As such it is O(n^2) so it may be slow for lots of objects

		float lateralSeparation = getLateralSeparation();
		float vertSeparation = getVerticalSeparation();
		int objectsCount = activeObjects.size();

		// Erase existing warnings
		collisionWarnings.clear();

		// Find new warnings
		for (int a = 0; a < objectsCount; a++)
		{
			AirspaceObject object1 = activeObjects.get(a);

			// Ignore non-solid objects and objects at 0 altitude
			if (!object1.isSolid() || object1.getAltitude() == 0)
				continue;

			Vector2D object1Position = object1.getPosition();
			float object1Altitude = object1.getAltitude();

			for (int b = a + 1; b < objectsCount; b++)
			{
				AirspaceObject object2 = activeObjects.get(b);

				// Ignore non-solid objects and objects at 0 altitude
				if (!object2.isSolid() || object2.getAltitude() == 0)
					continue;

				// Test collision
				if (object1Position.distanceTo(object2.getPosition()) < lateralSeparation &&
					Math.abs(object1Altitude - object2.getAltitude()) < vertSeparation)
				{
					// Add collision warning
					CollisionWarning warning = new CollisionWarning(object1, object2);
					collisionWarnings.add(warning);

					if (warning.hasCollided())
						gameOver = true;
				}
			}
		}
	}

	/**
	 * Called every so often to update the game state
	 *
	 * <p>
	 * Normally this is called every frame before the game is drawn to the screen.
	 *
	 * <p>
	 * The {@code delta} parameter allows the game to be frame-rate independent.
	 * This means the game should appear to run at the same speed regardless of the frame-rate.
	 * If you do not want this, you can use the value {@code 1 / fps} as the value for {@code delta}
	 *
	 * @param delta the time (in seconds) since the last refresh
	 */
	public void refresh(float delta)
	{
		ensureInitialized();
		
		boolean aircraftTakingOff = false;
		
		// Refresh all active objects
		for (AirspaceObject current : activeObjects)
		{	
			int waypointsHit = current.getWaypointsHit();
			
			current.refresh(delta);
			
			if(waypointsHit < current.getWaypointsHit())
				score += Constants.POINTS_FOR_WAYPOINT;
			
			if(current.isTakingOff())
			{
				// If the taking off aircraft is not already in the takingOffObjects list...
				if(takingOffObjects.indexOf(current) == -1)
				{
					// ... and if there is already an aircraft landing
					if(!landingObjects.isEmpty())
					{
						// Returns an error and undoes commands that caused the situation to occur
						System.err.println("Cannot take off from the airport while an aircraft is landing");
						current.setTakingOff(false);
						current.setWaitingToTakeOff(true);
						current.setTargetAltitude(0, true);
						aircraftTakingOff = false;
						return;
					}
					else
						takingOffObjects.add(current);
				}
				if(current.getAltitude() >= Constants.INITIAL_ALTITUDES.get(0))
				{
					current.setTakingOff(false);
					aircraftTakingOff = false;
					return;
				}
				else
					aircraftTakingOff = true;
			}
			
			// Adds circling aircraft to landingObjects (if it's not already in there)
			if(current.isCircling() && circlingObjects.indexOf(current) == -1)
				circlingObjects.add(current);
			
			// Adds landing aircraft to landingObjects (if it's not already in there)
			if(current.isLanding() && landingObjects.indexOf(current) == -1)
			{
				// If too many aircraft are landing
				if(landingObjects.size() >= Constants.MAX_AIRCRAFT_LANDING)
				{
					// Returns error and stops the aircraft most recently set to landing from descending
					System.err.println("Max landing aircraft is "+ Constants.MAX_AIRCRAFT_LANDING);
					current.setLanding(false);
					current.setTargetAltitude(current.getAltitude(), false);
					return;
				}
					
				// If an aircraft is already taking off
				if(!takingOffObjects.isEmpty())
				{
					// Return an error and revert game to correct state
					System.err.println("Cannot land while an aircraft is taking off");
					current.setLanding(false);
					landingObjects.clear();
					current.setTargetAltitude(0, true);
					return;
				}

				else
					landingObjects.add(current);
			}

		}

		if (!aircraftTakingOff)
			takingOffObjects.clear();
		
		
		// Cull any objects outside the game area
		cullObjects();

		// Add new aircraft
		AirspaceObject newObject = getObjectFactory().makeObject(this, delta);
		if (newObject != null)
			activeObjects.add(newObject);

		// Generate collision warnings + determine if game is over
		calculateCollisions();

		// Sort the list of aircraft
		Collections.sort(activeObjects, AltitudeComparator.INSTANCE);
	}

	
	/**
	 * Draws all objects in the airspace
	 *
	 * <p>
	 * This effectively calls {@link AirspaceObject#draw(Object)} on all objects.
	 *
	 * @param state any state information to be passed to the drawer
	 */
	public void draw(Object state)
	{
		// Draw forwards to draw lower planes first
		for (AirspaceObject current : activeObjects)
			current.draw(state);
	}

	/** Comparator comparing by altitude */
	private static class AltitudeComparator implements Comparator<AirspaceObject>
	{
		public static final AltitudeComparator INSTANCE = new AltitudeComparator();

		@Override
		public int compare(AirspaceObject o1, AirspaceObject o2)
		{
			return Float.compare(o1.getAltitude(), o2.getAltitude());
		}
	}
	
	/** Returns the value of the score */
	public int getScore(){
		return score;
	}
	
	/** Returns the list of AirspaceObjects that are landing at the airport */
	public ArrayList<AirspaceObject> getLandingObjects() {
		return landingObjects;
	}

	/** Returns the list of AirspaceObjects that are taking off from the airport */
	public ArrayList<AirspaceObject> getTakingOffObjects() {
		return takingOffObjects;
	}
}
