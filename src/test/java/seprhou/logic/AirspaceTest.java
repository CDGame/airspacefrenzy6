package seprhou.logic;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import seprhou.gui.Constants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * Tests for {@link Airspace}
 */
//@RunWith(JUnit4.class)
@RunWith(Enclosed.class)
public class AirspaceTest
{
	private static final Rectangle DIMENSIONS = new Rectangle(1000, 1000);
	private static final float SEPARATION = 200;
	
	@RunWith(JUnit4.class)
	public static class testAirportActivityLimits
	{
		AirspaceObject circlingObject1 = null;
		AirspaceObject circlingObject2 = null;
		AirspaceObject circlingObject3 = null;
		AirspaceObject waitingToTakeOffObject = null;
		
		Airspace airspace = generateAirspace(4);
		
		@Before
		public void initialSetUp()
		{
			// Initialise objects to be circling or landing
			for (AirspaceObject object : airspace.getActiveObjects())
			{
				if (circlingObject1 == null)
				{
					object.circling = true;
					object.altitude = 30000;
					object.position = Constants.AIRPORT;
					circlingObject1 = object;
				}
				else if (circlingObject2 == null)
				{
					object.circling = true;
					object.altitude = 35000;
					object.position = Constants.AIRPORT;
					circlingObject2 = object;
				}
				else if (circlingObject3 == null)
				{
					object.circling = true;
					object.altitude = 40000;
					object.position = Constants.AIRPORT;
					circlingObject3 = object;
				}
				else
				{
					object.waitingToTakeOff = true;
					object.altitude = 0;
					object.position = Constants.AIRPORT;
					waitingToTakeOffObject = object;
				}	
			}
			airspace.refresh(1);
		}
	
		@Test
		public void testInitialSetup() // UID: T006
		{
			// Test that the constants use correspond to the actual constants used in the game
			// -> If any of these tests fail, this class of tests should be updated to correspond to the new values
			assertThat(Constants.INITIAL_ALTITUDES.size(), is(3));
			assertThat(Constants.MAX_AIRCRAFT_LANDING, is(2));
			
			
			// Test the initial set up
			assertThat(airspace.getActiveObjects(), hasSize(4));
			assertThat(airspace.getCirclingObjects(), hasSize(3));
			assertThat(airspace.getLandingObjects(), hasSize(0));
			assertThat(airspace.getTakingOffObjects(), hasSize(0));
		}
		
		@Test
		public void testMaxLanding() // UID: T007
		{	
			// Set one of the aircraft circling to land
			circlingObject1.landing = true;
			airspace.refresh(1);
			
			assertThat(airspace.getActiveObjects(), hasSize(4));
			assertThat(airspace.getCirclingObjects(), hasSize(3));
			assertThat(airspace.getLandingObjects(), hasSize(1));
			assertThat(airspace.getTakingOffObjects(), hasSize(0));
			
			
			// Set another of the circling aircraft to land
			circlingObject2.landing = true;
			airspace.refresh(1);
			
			assertThat(airspace.getActiveObjects(), hasSize(4));
			assertThat(airspace.getCirclingObjects(), hasSize(3));
			assertThat(airspace.getLandingObjects(), hasSize(2));
			assertThat(airspace.getTakingOffObjects(), hasSize(0));
		
			
			// Attempt to set another of the circling aircraft to land
			circlingObject3.landing = true;
			airspace.refresh(1);
			
			assertThat(airspace.getActiveObjects(), hasSize(4));
			assertThat(airspace.getCirclingObjects(), hasSize(3));
			assertThat(airspace.getLandingObjects(), hasSize(2));
			assertThat(airspace.getTakingOffObjects(), hasSize(0));
			assertThat(circlingObject3.isLanding(), is(false));
			
		}
		
		@Test
		public void testLandingDuringTakeOff() // UID: T008
		{	
			// Start the taking off object ascending
			// takeOff() done manually as test object is not an instance of Aircraft 
			waitingToTakeOffObject.setTargetAltitude(Constants.INITIAL_ALTITUDES.get(0), true);
			waitingToTakeOffObject.waitingToTakeOff = false;
			waitingToTakeOffObject.takingOff = true;
			airspace.refresh(1);
			
			assertThat(airspace.getActiveObjects(), hasSize(4));
			assertThat(airspace.getCirclingObjects(), hasSize(3));
			assertThat(airspace.getLandingObjects(), hasSize(0));
			assertThat(airspace.getTakingOffObjects(), hasSize(1));			
			
			// Start an aircraft landing at the airport
			// Commands are those called when the relevant key is pressed in the GameArea class
			circlingObject1.landing = true;
			circlingObject1.setTargetAltitude(0, true);
			airspace.refresh(1);
			
			// Assert that aircraft is not landing
			assertThat(airspace.getActiveObjects(), hasSize(4));
			assertThat(airspace.getCirclingObjects(), hasSize(3));
			assertThat(airspace.getLandingObjects(), hasSize(0));
			assertThat(circlingObject1.isCircling(), is(true));
			assertThat(circlingObject1.isLanding(), is(false));
			
			// Assert that taking off aircraft is still taking off
			assertThat(airspace.getTakingOffObjects(), hasSize(1));
			assertThat(waitingToTakeOffObject.isTakingOff(), is(true));
			assertThat(waitingToTakeOffObject.isWaitingToTakeOff(), is(false));
			
		}
		
		@Test
		public void testTakeOffDuringLanding() // UID: T009
		{
			// Start the landing object descending
			// Commands are those called when the relevant key is pressed in the GameArea class
			circlingObject1.setTargetAltitude(0, true);
			circlingObject1.landing = true;
			airspace.refresh(1);

			assertThat(airspace.getActiveObjects(), hasSize(4));
			assertThat(airspace.getCirclingObjects(), hasSize(3));
			assertThat(airspace.getLandingObjects(), hasSize(1));
			assertThat(airspace.getTakingOffObjects(), hasSize(0));

			// Start an aircraft taking off at the airport
			// takeOff() done manually as test object is not an instance of Aircraft 
			waitingToTakeOffObject.setTargetAltitude(30000, true);
			waitingToTakeOffObject.waitingToTakeOff = false;
			waitingToTakeOffObject.takingOff = true;
			airspace.refresh(1);

			// Assert that aircraft is not taking off
			assertThat(airspace.getActiveObjects(), hasSize(4));
			assertThat(airspace.getTakingOffObjects(), hasSize(0));
			assertThat(waitingToTakeOffObject.isTakingOff(), is(false));
			assertThat(waitingToTakeOffObject.isWaitingToTakeOff(), is(true));
			assertThat(waitingToTakeOffObject.getTargetAltitude(), is((float)0));
			
			// Assert that landing aircraft is still landing
			assertThat(airspace.getCirclingObjects(), hasSize(3));
			assertThat(airspace.getLandingObjects(), hasSize(1));
			assertThat(circlingObject1.isCircling(), is(true));
			assertThat(circlingObject1.isLanding(), is(true));

		}
	}
	
	
	public static class generalTests
	{
		@Test
		public void testActiveObjectsOrder() // UID: T010
		{
			Airspace airspace = generateAirspace(1000, 4000, 3000, 10, 4000);
			airspace.refresh(1);

			// Ensure objects are in order
			Collection<AirspaceObject> activeObjects = airspace.getActiveObjects();
			assertThat(activeObjects, hasSize(5));

			List<Float> altitudes = new ArrayList<>();
			for (AirspaceObject object : activeObjects)
				altitudes.add(object.getAltitude());

			assertThat(altitudes, contains(10f, 1000f, 3000f, 4000f, 4000f));
		}

		@Test
		public void testCollisionWarningsAltitude() // UID: T011
		{
			Airspace airspace = generateAirspace(200, 400, 800, 100, 400, 0);
			airspace.refresh(1);

			/*
			Warnings between:
			 100 - 200
			 400 - 400
			 */

			// Test against actual collision warnings
			Collection<CollisionWarning> warnings = airspace.getCollisionWarnings();
			assertThat(warnings, hasSize(2));

			for (CollisionWarning warning : warnings)
			{
				// Check which warning it is
				if (warning.getObject1().getAltitude() == 400)
				{
					assertThat(warning.getObject2().getAltitude(), is(400f));
				}
				else if (warning.getObject1().getAltitude() == 200)
				{
					assertThat(warning.getObject2().getAltitude(), is(100f));
				}
				else
				{
					assertThat(warning.getObject1().getAltitude(), is(100f));
					assertThat(warning.getObject2().getAltitude(), is(200f));
				}
			}
		}

		@Test
		public void testGameOver() // UID : T012
		{
			Airspace airspace1 = generateAirspace(1, 5000);
			Airspace airspace2 = generateAirspace(1, 2);

			// Airspace 1 is bad, airspace 2 is good
			airspace1.refresh(1);
			airspace2.refresh(1);
			assertThat(airspace1.isGameOver(), is(false));
			assertThat(airspace2.isGameOver(), is(true));
		}

		@Test
		public void testFindAircraft() // UID : T013
		{
			Vector2D obj1 = new Vector2D(100, 100);
			Vector2D obj2 = new Vector2D(200, 200);
			Vector2D obj3 = new Vector2D(300, 300);
			Airspace airspace = generateAirspace(obj1, obj2, obj3);
			airspace.refresh(1);

			// Test find aircraft clicking
			AirspaceObject found = airspace.findAircraft(obj2);
			assertThat(found, is(notNullValue()));
			assertThat(found.getPosition(), is(obj2));

			// Test none in range
			AirspaceObject found2 = airspace.findAircraft(new Vector2D(5000, 400));
			assertThat(found2, is(nullValue()));

			// Test circle
			List<Vector2D> positions = objectsToPositions(airspace.findAircraft(obj3, 200));
			assertThat(positions, containsInAnyOrder(obj2, obj3));
		}

		@Test
		public void testFindAircraftAltitude()
		{
			Airspace airspace = generateAirspace(1, 2, 3);
			airspace.refresh(1);

			// Aircraft search should return 3
			AirspaceObject found = airspace.findAircraft(Vector2D.ZERO);
			assertThat(found, is(notNullValue()));
			assertThat(found.getAltitude(), is(3f));
		}

		@Test
		public void testObjectCulling()
		{
			Vector2D objectOk = new Vector2D(100, 100);
			Vector2D objectCulled = new Vector2D(2000, 100);
			Airspace airspace = generateAirspace(objectOk, objectCulled);

			// Validate state before refresh
			assertThat(airspace.getCulledObjects(), hasSize(0));
			assertThat(airspace.getActiveObjects(), hasSize(2));

			// After one iteration
			airspace.refresh(1);
			assertThat(airspace.getCulledObjects(), hasSize(1));
			assertThat(airspace.getActiveObjects(), hasSize(1));

			// Culled object is purged after another iteration
			airspace.refresh(1);
			assertThat(airspace.getCulledObjects(), hasSize(0));
			assertThat(airspace.getActiveObjects(), hasSize(1));
		}
		
	}
	
	
	public static class airportTests
	{
		@Test
		public void testObjectCircling() // UID: T014
		{
			Airspace airspace = generateAirspace(30000, 35000);
			airspace.refresh(1);
			assertThat(airspace.getActiveObjects(), hasSize(2));
			assertThat(airspace.getCirclingObjects(), hasSize(0));
			assertThat(airspace.getLandingObjects(), hasSize(0));
			
			// Select one of the generated aircraft to be circling the airport
			for (AirspaceObject object : airspace.getActiveObjects())
			{
				object.position = Constants.AIRPORT;
				object.circling = true;
				break;
			}
			
			assertThat(airspace.getActiveObjects(), hasSize(2));
			assertThat(airspace.getCirclingObjects(), hasSize(0));
			assertThat(airspace.getLandingObjects(), hasSize(0));
			
			airspace.refresh(1);

			assertThat(airspace.getActiveObjects(), hasSize(2));
			assertThat(airspace.getCirclingObjects(), hasSize(1));
			assertThat(airspace.getLandingObjects(), hasSize(0));
			
		}
	
		@Test
		public void testObjectLanding() // UID: T015
		{
			AirspaceObject landingObject = null;
			Airspace airspace = generateAirspace((float) 30000, 35000);
			airspace.refresh(1);
			assertThat(airspace.getActiveObjects(), hasSize(2));
			assertThat(airspace.getLandingObjects(), hasSize(0));
			
			// Select one of the generated aircraft to be circling the airport
			for (AirspaceObject object : airspace.getActiveObjects())
			{
				object.position = Constants.AIRPORT;
				object.circling = true;
				landingObject = object;
				break;
			}
			
			// Commands triggered by player's controls
			landingObject.setTargetAltitude(0, true);
			landingObject.landing = true;
			
			airspace.refresh(1);
			
			// Validate state before aircraft descends
			assertThat(airspace.getActiveObjects(), hasSize(2));
			assertThat(airspace.getCirclingObjects(), hasSize(1));
			assertThat(airspace.getLandingObjects(), hasSize(1));
			assertThat(airspace.getCulledObjects(), hasSize(0));
			
			while (landingObject.altitude > 1000)
				airspace.refresh(1);
			
			// Validate state part way through aircraft's descent
			assertThat(airspace.getActiveObjects(), hasSize(2));
			assertThat(airspace.getCirclingObjects(), hasSize(1));
			assertThat(airspace.getLandingObjects(), hasSize(1));
			assertThat(airspace.getCulledObjects(), hasSize(0));

			while (landingObject.altitude > 0)
				airspace.refresh(1);

			// After aircraft has reached altitude 0
			assertThat(airspace.getActiveObjects(), hasSize(1));
			assertThat(airspace.getCirclingObjects(), hasSize(0));
			assertThat(airspace.getLandingObjects(), hasSize(0));
			assertThat(airspace.getCulledObjects(), hasSize(1));			
		}
		
		@Test
		public void testObjectTakingOff() // UID: T016
		{	
			// Generate an airspace containing an object that has inital altitude of 0 and is ascending
			AirspaceObject takingOffObject = null;
			float targetAltitude = Constants.INITIAL_ALTITUDES.get(0);
			Airspace airspace = generateAirspace((float) 0);
			assertThat(airspace.getActiveObjects(), hasSize(1));
			assertThat(airspace.getTakingOffObjects(), hasSize(0));
 
			// Initialise the AirspaceObject
			for (AirspaceObject object : airspace.getActiveObjects())
			{
				takingOffObject = object; 
				break;
			}
			
			// Initialise takingOffObject -> Start it ascending
			// takeOff() done manually as test object is not an instance of Aircraft 
			takingOffObject.setWaitingToTakeOff(false);
			takingOffObject.setTakingOff(true);
			takingOffObject.setTargetAltitude(targetAltitude, true);
			
			airspace.refresh(1);
			
			// Wait and refresh while the aircraft ascends
			while (takingOffObject.getAltitude() < targetAltitude)
			{
				assertThat(airspace.getTakingOffObjects(), hasSize(1));
				airspace.refresh(1);
			}

			// Once the aircraft is at an flying altitude
			airspace.refresh(1);
			
			assertThat(airspace.getActiveObjects(), hasSize(1));
			assertThat(airspace.getTakingOffObjects(), hasSize(0));
			assertThat(takingOffObject.isTakingOff(), is(false));
			
		}
	
		
	}
	
	/** Converts a list of objects to a list of positions */
	private static List<Vector2D> objectsToPositions(Iterable<AirspaceObject> objects)
	{
		List<Vector2D> positions = new ArrayList<>();

		for (AirspaceObject object : objects)
			positions.add(object.getPosition());

		return positions;
	}

	/** Generates an airspace with the given number of objects */
	private static Airspace generateAirspace(final int objectCount)
	{
		// Configure airspace
		Airspace airspace = new Airspace();
		airspace.setDimensions(DIMENSIONS);
		airspace.setLateralSeparation(SEPARATION);
		airspace.setVerticalSeparation(SEPARATION);
		airspace.setObjectFactory(new AirspaceObjectFactory()
		{
			private int objectsLeft = objectCount;

			@Override
			public AirspaceObject makeObject(Airspace airspace, float delta)
			{
				if (objectsLeft > 0)
				{
					objectsLeft--;
					return new AirspaceObjectMock();
				}

				return null;
			}
		});

		// Generate some objects
		for (int i = 0; i < objectCount; i++)
			airspace.refresh(0);

		// Solidify them
		for (AirspaceObject object : airspace.getActiveObjects())
			((AirspaceObjectMock) object).solid = true;

		// The game should not be over
		assertThat(airspace.isGameOver(), is(false));
		return airspace;
	}

	/** Generates an airspace with some objects with the given positions */
	private static Airspace generateAirspace(Vector2D... objects)
	{
		Airspace airspace = generateAirspace(objects.length);

		// Set their positions
		int i = 0;
		for (AirspaceObject object : airspace.getActiveObjects())
		{
			object.position = objects[i];
			i++;
		}

		return airspace;
	}

	/** Generates an airspace with some objects with the given altitudes */
	private static Airspace generateAirspace(float... altitudes)
	{
		Airspace airspace = generateAirspace(altitudes.length);

		// Set their positions
		int i = 0;
		for (AirspaceObject object : airspace.getActiveObjects())
		{
			object.altitude = altitudes[i];
			object.setTargetAltitude(altitudes[i], object.isLanding());
			i++;
		}

		return airspace;
	}

	/** Fake {@link AirspaceObject} class used for testing */
	private static class AirspaceObjectMock extends AirspaceObject
	{
		public boolean solid;

		@Override public boolean isSolid() { return solid; }
		@Override public void draw(Object state) { }
		@Override public float getSize() { return 64; }
		@Override public float getAscentRate() { return 100; }
		@Override public float getMinSpeed() { return 0; }
		@Override public float getMaxSpeed() { return 100; }
		@Override public float getMinAltitude() { return 0; }
		@Override public float getMaxAltitude() { return 40000; }
		@Override public float getMaxAcceleration() { return 100; }
		@Override public float getMaxTurnRate() { return 1; }
		@Override public int getWaypointsHit() { return 0; }
	}
	
	
}
