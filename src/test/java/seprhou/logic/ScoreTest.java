package seprhou.logic;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import seprhou.logic.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;


/**
 * Tests for {@link Airspace}
 */
@RunWith(JUnit4.class)
public class ScoreTest
{
	// scores.dat file needs to be cleared before testing
	
    HighScores highScores = new HighScores();
	
	@Test
	public void testAddingScore()
	{
		// Validate state before execution
		assertThat(highScores.getScores(), hasSize(0));

		// After adding one score
		highScores.addScore("Player1", 100);
		assertThat(highScores.getScores(), hasSize(1));

		// After adding two scores
		highScores.addScore("Player2", 150);
		assertThat(highScores.getScores(), hasSize(2));

		// After adding three scores
		highScores.addScore("Player3", 50);
		assertThat(highScores.getScores(), hasSize(3));
	}

	@Test
	public void testScoreOrder()
	{
		// First player in the list
		assertThat(highScores.getScores().get(0).getNaam(), is("Player2"));
		assertThat(highScores.getScores().get(0).getScore(), is(150));

		// Second player in the list
		assertThat(highScores.getScores().get(1).getNaam(), is("Player1"));
		assertThat(highScores.getScores().get(1).getScore(), is(100));

		// Third player in the list
		assertThat(highScores.getScores().get(2).getNaam(), is("Player3"));
		assertThat(highScores.getScores().get(2).getScore(), is(50));
	}

	@Test
	public void testMaximumPlayers()
	{
		// Validate state before execution
		assertThat(highScores.getScores(), hasSize(3));

		// Adding 7 players to the high score list
		highScores.addScore("Player4", 10);
		assertThat(highScores.getScores(), hasSize(4));
		highScores.addScore("Player5", 20);
		assertThat(highScores.getScores(), hasSize(5));
		highScores.addScore("Player6", 30);
		assertThat(highScores.getScores(), hasSize(6));
		highScores.addScore("Player7", 40);
		assertThat(highScores.getScores(), hasSize(7));
		highScores.addScore("Player8", 50);
		assertThat(highScores.getScores(), hasSize(8));
		highScores.addScore("Player9", 60);
		assertThat(highScores.getScores(), hasSize(9));
		highScores.addScore("Player10", 70);
		assertThat(highScores.getScores(), hasSize(10));

		// Adding 11th and 12th players after which there should still be 10 players in the list
		highScores.addScore("Player11", 80);
		assertThat(highScores.getScores(), hasSize(10));
		highScores.addScore("Player12", 90);
		assertThat(highScores.getScores(), hasSize(10));		
	}	
	
}
